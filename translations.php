<?php
$file = basename(__FILE__, '.php');
require './config/setup.php';
require './library/functions.php';
require_once("./library/languageswitch.php");
$langGlobal = getPageContent($file);
if (!isset($_SESSION['CURR_USR_UID']) || $_SESSION['CURR_USR_TYPE'] !== 'admin') {
    header("Location: index.php");
}

$select_area = $db -> get('translation_area');

require './views/template/header.php';
require './views/' . $file . "_view.php";
require './views/template/footer.php';

?>