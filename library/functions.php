<?php

function htmlMail($to, $cc, $subject, $contentHtml, $title,$logo=null) {
    global $rt_path, $lang_file;
    $url = SERVER_PATH_EMAIL;
    $style = file_get_contents($rt_path . "css/email.css");
 
    $recipients = $to;
    $headers = array('From' => SMTP_EMAIL_FROM, 'To' => $to, 'Subject' => $subject);
    $headers["Content-Type"] = 'text/html; charset=UTF-8';
    $headers["Content-Transfer-Encoding"] = "8bit";
    if (trim($cc) != "") {
        $recipients = $to . ", " . $cc;
        $headers["Cc"] = $cc;
    }
   if($logo == null){
    $logo =  $url . 'images/hero_and_logo/logo.png';
   }
    $html = '<html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <link rel="stylesheet" href="' . $url . 'font_brandon/fonts.css" type="text/css" charset="utf-8" />  
                           
                        <style>' . $style . '</style>
                    </head>
                    <body>                        
                        <div id="mailwrapper">
                            <div id="header"> 
                                <div id="logo"><a href="' . $url . '" target="_blank"><img src="'.$logo.'" /></a></div>
                               
                            </div>
                            <div id="headline">' . $title . '</div>
                            <div id="inhalt" style="margin-top: 50px;">
                                ' . $contentHtml . '
                            </div>
                            <div id="footer">
                                <p style="float: left;">©'.date("Y").' all rights reserved by Healthcare X.0<sup>®</sup> GmbH</p>
                                <p style="float: right;"><a href="">www.patientassist.de</a></p>
                                <div style="clear:both;"></div>
                            </div>
                        </div>
                    </body>
                </html>';  // HTML version of the email    
    // Setting the body of the email

    $mime = new Mail_mime();
    $mime->setHTMLBody($html);
    $mimeparams = array();

    $mimeparams['text_encoding'] = "8bit";
    $mimeparams['text_charset'] = "UTF-8";
    $mimeparams['html_charset'] = "UTF-8";
    $mimeparams['head_charset'] = "UTF-8";

    $body = $mime->get($mimeparams);
    $headers2 = $mime->headers($headers);

    $smtp = Mail::factory('smtp', array(
                'host' => SMTP_HOST,
                'port' => SMTP_PORT,
                'auth' => true,
                'username' => SMTP_USER,
                'password' => SMTP_PASSW
                    )
    );

    $mail = $smtp->send($recipients, $headers2, $body);

}


function generateStrongPassword($length = 10, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') !== false)
        $sets[] = '+-=_:.;!?@#$%^&*()';
    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if (!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function checkSelectedOpt($val, $db_val, $text) {
    $val2 = str_replace(' ', '', $val);
    $db_val2 = str_replace(' ', '', $db_val);
    $val2 = ltrim(rtrim($val2));
    $db_val2 = ltrim(rtrim($db_val2));
    $val3 = ltrim(rtrim($val));
     $val3 = str_replace('"', '&quot;', $val3);
    if ($val2 == $db_val2) {
        echo "<option value=\"$val\" selected=\"selected\">" . $text . "</option >";
    } else {
        echo "<option value=\"$val3\">{$text}</option >";
    }
}
function checkStatus($db_val) {

    $db_val2 = str_replace(' ', '', $db_val);
    $db_val2 = ltrim(rtrim($db_val2));
    if($db_val2 == '2'){
      echo "<span   class=status_green></span>";
    }  
      if($db_val2 == '1'){
      echo "<span   class=status_orange></span>";
    }
      if($db_val2 == '0'){
      echo "<span  class=status_red></span>";
    }
}

function convertDate($sdate) {   
    global $language;
    //echo "lang".$language;
    if (strpos($sdate, '-') !== false && $language == "de") {
        $d_arr = explode("-", $sdate);
        $new_date = $d_arr[2].".".$d_arr[1].".".$d_arr[0];
    } else {
        $d_arr = explode("-", $sdate);
        $new_date = $d_arr[1]."/".$d_arr[2]."/".$d_arr[0];
    }    
    return $new_date;
}

?>