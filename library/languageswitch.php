<?php
//require './config/setup.php';
if(!isset($_SESSION['LANGUAGE'])){
       $_SESSION['LANGUAGE'] = 'de' ;
    }
function getPageContent($file){
  global $db;
  $language= $_SESSION['LANGUAGE'] ;
  $langGlobal = array();  
 $query= "select i.name,t.translation 
    from  translations t left join translation_items i on t.translation_item_id = i.id 
    where t.language_code = ? and i.file in (?,?,?,?)";
  $langResult = $db->rawQuery($query,array($language,'institution_header','header','footer',$file));
  //$langResult = $db->where()
  foreach ($langResult as $row)
  {
    $langGlobal[$row['name']] = $row['translation'];
  }
  return $langGlobal;
  
}

?>